#!/bin/bash

INSTALLDIR="installation"
CORES=`nproc`
MARCH="core2"

cd sources/fftw-2.1.3/
if [ -d "$INSTALLDIR" ]; then
   echo "Existeix installacio previa..."
   make clean
   rm -rf $INSTALLDIR
fi

mkdir $INSTALLDIR
echo "############ environment: START #############"
echo "env CFLAGS='-O3 -march=$MARCH' ./configure --prefix=$PWD/$INSTALLDIR --enable-float"
env CFLAGS="-O3 -march=$MARCH" ./configure --prefix=$PWD/$INSTALLDIR --enable-float
echo "############ environment: END  ##############"

echo "########### MAKE -j$CORES ###########"
make -j$CORES

echo "########### MAKE  INSTALL ###########"
make install

echo "########### Setting environment ##########" 
export PCA=$PWD/$INSTALLDIR

cd ../../

echo -e "\n\n\n"
echo "Please add the follow environment variable at bashrc or similar: PCA=$PCA"