#define PITAGORAS(x,y,z) sqrt(x*x + y*y + z*z)                                  

#include "structures.h"

/* VARIABLES GLOBALS */
struct Structure TS;
fftw_real *gr;
int g_size;
float g_span;

int count = 0;
pthread_mutex_t mut = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
#define MAXTHREADS 4


void assign_charges( struct Structure This_Structure ) {
/************/
  /* Counters */
  int	residue , atom ;
/************/
  for( residue = 1 ; residue <= This_Structure.length ; residue ++ ) {
    for( atom = 1 ; atom <= This_Structure.Residue[residue].size ; atom ++ ) {
      This_Structure.Residue[residue].Atom[atom].charge = 0.0 ;
      /* peptide backbone */
      if( strcmp( This_Structure.Residue[residue].Atom[atom].atom_name , " N  " ) == 0 ) {
        if( strcmp( This_Structure.Residue[residue].res_name , "PRO" ) == 0 ) {
          This_Structure.Residue[residue].Atom[atom].charge = -0.10 ;
        } else {
          This_Structure.Residue[residue].Atom[atom].charge =  0.55 ;
          if( residue == 1 ) This_Structure.Residue[residue].Atom[atom].charge = 1.00 ;
        }
      }
      if( strcmp( This_Structure.Residue[residue].Atom[atom].atom_name , " O  " ) == 0 ) {
        This_Structure.Residue[residue].Atom[atom].charge = -0.55 ;
        if( residue == This_Structure.length  ) This_Structure.Residue[residue].Atom[atom].charge = -1.00 ;
      }
      /* charged residues */

      if( ( strcmp( This_Structure.Residue[residue].res_name , "ARG" ) == 0 ) && ( strncmp( This_Structure.Residue[residue].Atom[atom].atom_name , " NH" , 3 ) == 0 ) ) This_Structure.Residue[residue].Atom[atom].charge =  0.50 ;
      if( ( strcmp( This_Structure.Residue[residue].res_name , "ASP" ) == 0 ) && ( strncmp( This_Structure.Residue[residue].Atom[atom].atom_name , " OD" , 3 ) == 0 ) ) This_Structure.Residue[residue].Atom[atom].charge = -0.50 ;
      if( ( strcmp( This_Structure.Residue[residue].res_name , "GLU" ) == 0 ) && ( strncmp( This_Structure.Residue[residue].Atom[atom].atom_name , " OE" , 3 ) == 0 ) ) This_Structure.Residue[residue].Atom[atom].charge = -0.50 ;
      if( ( strcmp( This_Structure.Residue[residue].res_name , "LYS" ) == 0 ) && ( strcmp( This_Structure.Residue[residue].Atom[atom].atom_name , " NZ " ) == 0 ) ) This_Structure.Residue[residue].Atom[atom].charge =  1.00 ;
    }
  }
/************/
}


inline void incrementCount() {
  pthread_mutex_lock(&mut);
  ++count;
  pthread_mutex_unlock(&mut);
}

inline void decrementCount() {
  pthread_mutex_lock(&mut);
  --count;
  if (count < MAXTHREADS) pthread_cond_broadcast(&cond);
  pthread_mutex_unlock(&mut);
}

/************************/                                     

  #define EPSILONBASE(index)                                                                \
  if(!(distance[index] > 6)) epsilon[index] = 4;                                            \
  else if(distance[index] >= 8) epsilon[index] = 80;                                        \
  else epsilon[index] = 38*distance[index]-224;                                             \

  #define EPSILON(index)                                                                      \
  if(distance[0] < 2.0) distance[index] = 2.0;                                                 \
  EPSILONBASE(index)                                                                           \
  gr[gaddress(x,y,z+index,g_size)] += (fftw_real)(charge/(epsilon[index]*distance[index])); 

/*
void *electric_field_thread(void *n){
  //Declaració de variables
  int x,y,z,residue,atom;
  float x_centre,y_centre,z_centre,distance,phi,epsilon;
  register float myX,myY,myZ;
  x = (int)n;
  x_centre = gcentre(x,g_span,g_size);
  printf(".");
  for(y = 0; y < g_size; y++){
    y_centre = gcentre(y,g_span,g_size);
    for(z = 0; z < g_size; z++){
      z_centre = gcentre(z,g_span,g_size);
      phi = 0 ;
      for(residue = 1; residue <= TS.length; residue++){
        for(atom = 1; atom <= TS.Residue[residue].size; atom++ ){
          if(TS.Residue[residue].Atom[atom].charge != 0){
            myX = TS.Residue[residue].Atom[atom].coord[1] - x_centre;
            myY = TS.Residue[residue].Atom[atom].coord[2] - y_centre;
            myZ = TS.Residue[residue].Atom[atom].coord[3] - z_centre;
            distance = PITAGORAS(myX,myY,myZ);

            if(distance < 2.0) distance = 2.0;
            if(!(distance > 6)) {epsilon = 4;}
            else if(distance >= 8) {epsilon = 80;}
            else {epsilon = 38*distance - 224;}
            phi += ( TS.Residue[residue].Atom[atom].charge / ( epsilon * distance ) ) ;
          }
        }
      }
      gr[gaddress(x,y,z,g_size)] = (fftw_real)phi ;
    }
  }
  decrementCount();
  return NULL;
}*/

float *pre_centres;

void *electric_field_thread(void *n){
  //Declaració de variables
  int x,y,z,residue,atom;
  float x_centre,y_centre,charge,xprod,yprod,zprod;
  float *z_centre,*distance,*epsilon;
  __m128 xs,ys,zs,myX,myY,myZ,xAtom,yAtom,zAtom;
  __m128 eps, chr;

  float auxX, auxY, tmpY;

  __m128 auxEps = _mm_set1_ps(2.0);

  struct Atom *aux;

  x = (int)n;
  x_centre = pre_centres[x];
  posix_memalign((void**)&z_centre,16,4*sizeof(float));
  posix_memalign((void**)&distance,16,4*sizeof(float));
  posix_memalign((void**)&epsilon,16,4*sizeof(float));
  
  printf(".");

  for(residue = 1; residue <= TS.length; residue++){
    for(atom = 1; atom <= TS.Residue[residue].size; atom++){
      aux = &TS.Residue[residue].Atom[atom];
      zAtom = _mm_set1_ps(TS.Residue[residue].Atom[atom].coord[3]);
      charge = TS.Residue[residue].Atom[atom].charge;
      if(charge != 0){
        auxX = TS.Residue[residue].Atom[atom].coord[1] - x_centre;
        auxX *= auxX;
        chr = _mm_set1_ps(charge);

        tmpY = TS.Residue[residue].Atom[atom].coord[2];

        for(y = 0; y < g_size; y++){
          y_centre = pre_centres[y]; //gcentre(y,g_span,g_size);
          auxY = tmpY - y_centre;
          auxY *= auxY;
          auxY += auxX;
          ys = _mm_set1_ps(auxY);

          for(z = 0; z < g_size-3; z += 4){
            zs = _mm_load_ps(&pre_centres[z]);
            zs = _mm_sub_ps(zAtom,zs);
            zs = _mm_mul_ps(zs,zs);
            zs = _mm_add_ps(zs,ys);
            zs = _mm_sqrt_ps(zs);
            zs = _mm_max_ps(zs, auxEps);   //if(distance[index] < 2.0) distance[index] = 2.0;                                                      

            _mm_store_ps(distance,zs);
            EPSILONBASE(0)
            EPSILONBASE(1)
            EPSILONBASE(2)
            EPSILONBASE(3)

            zs = _mm_load_ps(epsilon); //(charge/(epsilon[0..3]*distance[0..3]));
            eps = _mm_load_ps(distance);
            zs = _mm_mul_ps(zs, eps);
            zs = _mm_div_ps(chr, zs);
            _mm_store_ps(epsilon,zs);

            gr[gaddress(x,y,z+0,g_size)] += (fftw_real)epsilon[0]; 
            gr[gaddress(x,y,z+1,g_size)] += (fftw_real)epsilon[1];  
            gr[gaddress(x,y,z+2,g_size)] += (fftw_real)epsilon[2];  
            gr[gaddress(x,y,z+3,g_size)] += (fftw_real)epsilon[3];  
          }
          for(; z < g_size; z++){
            z_centre[0] = pre_centres[z]; //gcentre(z,g_span,g_size);
            xprod = aux->coord[1] - x_centre;
            yprod = aux->coord[2] - y_centre;
            zprod = aux->coord[3] - z_centre[0];
            distance[0] = sqrt(xprod*xprod + yprod*yprod + zprod*zprod);
            EPSILON(0)
          }
        }
      }
    }
  }
  //pthread_exit(NULL); // PETA en i5-2450M
  decrementCount();
  return NULL;
}

void electric_field( struct Structure This_Structure , float grid_span , int grid_size , fftw_real *grid ) {
  //Inicialització/declaració de variables
  int i, rc;
  pthread_t threads[grid_size];
  g_span = grid_span;
  g_size = grid_size;
  gr = grid;
  TS = This_Structure;

  setvbuf(stdout, (char *)NULL, _IONBF, 0) ;

  posix_memalign((void**)&pre_centres,16,g_size*sizeof(float));

  for(i = 0; i < g_size; ++i) pre_centres[i] = gcentre(i, g_span, g_size);

  printf("  electric field calculations ( one dot / grid sheet ) ");

  for (i = 0; i < grid_size; ++i) {
    pthread_mutex_lock(&mut);
    while (count >= MAXTHREADS) pthread_cond_wait(&cond, &mut);
    pthread_mutex_unlock(&mut);

    rc = pthread_create(&threads[i],NULL,electric_field_thread,(void*)i);
    if (rc) {
      printf("ERROR; return code from pthread create() is %d", rc);
      exit(-1);
    }
    else incrementCount();
  }

  for(i = 0; i < grid_size; i++) pthread_join(threads[i],NULL);
  printf("\n");

  return ;
}

void electric_field2( struct Structure This_Structure , float grid_span , int grid_size , fftw_real *grid ) {
/************/
  /* Counters */
  int residue , atom ;

  /* Co-ordinates */
  int x , y , z ;
  float   x_centre , y_centre , z_centre ;

  /* Variables */
  float   distance ;
  float   phi , epsilon ;

/************/
  for( x = 0 ; x < grid_size ; x ++ ) {
    for( y = 0 ; y < grid_size ; y ++ ) {
      for( z = 0 ; z < grid_size ; z ++ ) {
        grid[gaddress(x,y,z,grid_size)] = (fftw_real)0 ;
      }
    }
  }
/************/

  setvbuf( stdout , (char *)NULL , _IONBF , 0 ) ;
  printf( "  electric field calculations ( one dot / grid sheet ) " ) ;
  for( x = 0 ; x < grid_size ; x ++ ) {
    printf( "." ) ;
    x_centre  = gcentre( x , grid_span , grid_size ) ;
    for( y = 0 ; y < grid_size ; y ++ ) {
      y_centre  = gcentre( y , grid_span , grid_size ) ;
      for( z = 0 ; z < grid_size ; z ++ ) {
        z_centre  = gcentre( z , grid_span , grid_size ) ;
        phi = 0 ;
        for( residue = 1 ; residue <= This_Structure.length ; residue ++ ) {
          for( atom = 1 ; atom <= This_Structure.Residue[residue].size ; atom ++ ) {
            if( This_Structure.Residue[residue].Atom[atom].charge != 0 ) {
              distance = pythagoras( This_Structure.Residue[residue].Atom[atom].coord[1] , This_Structure.Residue[residue].Atom[atom].coord[2] , This_Structure.Residue[residue].Atom[atom].coord[3] , x_centre , y_centre , z_centre ) ;
              if( distance < 2.0 ) distance = 2.0 ;
              if( distance >= 2.0 ) {
                if( distance >= 8.0 ) {
                  epsilon = 80 ;
                } else { 
                  if( distance <= 6.0 ) { 
                    epsilon = 4 ;
                  } else {
                    epsilon = ( 38 * distance ) - 224 ;
                  }
                }
                phi += ( This_Structure.Residue[residue].Atom[atom].charge / ( epsilon * distance ) ) ;
              }
            }
          }
        }
        grid[gaddress(x,y,z,grid_size)] = (fftw_real)phi ;
      }
    }
  }
  printf( "\n" ) ;
/************/
  return ;
}


/************************/

void electric_point_charge( struct Structure This_Structure , float grid_span , int grid_size , fftw_real *grid ) {
/************/
  /* Counters */
  int	residue , atom ;

  /* Co-ordinates */
  int	x , y , z ;
  int	x_low , x_high , y_low , y_high , z_low , z_high ;
  float		a , b , c ;
  float		x_corner , y_corner , z_corner ;
  float		w ;

  /* Variables */
  float		one_span ;
/************/
  for( x = 0 ; x < grid_size ; x ++ ) {
    for( y = 0 ; y < grid_size ; y ++ ) {
      for( z = 0 ; z < grid_size ; z ++ ) {
        grid[gaddress(x,y,z,grid_size)] = (fftw_real)0 ;
      }
    }
  }
/************/
  one_span = grid_span / (float)grid_size ;
  for( residue = 1 ; residue <= This_Structure.length ; residue ++ ) {
    for( atom = 1 ; atom <= This_Structure.Residue[residue].size ; atom ++ ) {
      if( This_Structure.Residue[residue].Atom[atom].charge != 0 ) {
        x_low = gord( This_Structure.Residue[residue].Atom[atom].coord[1] - ( one_span / 2 ) , grid_span , grid_size ) ;
        y_low = gord( This_Structure.Residue[residue].Atom[atom].coord[2] - ( one_span / 2 ) , grid_span , grid_size ) ;
        z_low = gord( This_Structure.Residue[residue].Atom[atom].coord[3] - ( one_span / 2 ) , grid_span , grid_size ) ;

        x_high = x_low + 1 ;
        y_high = y_low + 1 ;
        z_high = z_low + 1 ;

        a = This_Structure.Residue[residue].Atom[atom].coord[1] - gcentre( x_low , grid_span , grid_size ) - ( one_span / 2 ) ;
        b = This_Structure.Residue[residue].Atom[atom].coord[2] - gcentre( y_low , grid_span , grid_size ) - ( one_span / 2 ) ;
        c = This_Structure.Residue[residue].Atom[atom].coord[3] - gcentre( z_low , grid_span , grid_size ) - ( one_span / 2 ) ;

        for( x = x_low ; x <= x_high  ; x ++ ) {
          x_corner = one_span * ( (float)( x - x_high ) + .5 ) ;

          for( y = y_low ; y <= y_high  ; y ++ ) {
            y_corner = one_span * ( (float)( y - y_high ) + .5 ) ;

            for( z = z_low ; z <= z_high  ; z ++ ) {
              z_corner = one_span * ( (float)( z - z_high ) + .5 ) ;
              w = ( ( x_corner + a ) * ( y_corner + b ) * ( z_corner + c ) ) / ( 8.0 * x_corner * y_corner * z_corner ) ;
              grid[gaddress(x,y,z,grid_size)] += (fftw_real)( w * This_Structure.Residue[residue].Atom[atom].charge ) ;
            }
          }
        }
      }
    }
  }
/************/
  return ;
}



/************************/



void electric_field_zero_core( int grid_size , fftw_real *elec_grid , fftw_real *surface_grid , float internal_value ) {
/************/
  /* Co-ordinates */
  int	x , y , z ;
/************/
  for( x = 0 ; x < grid_size ; x ++ ) {
    for( y = 0 ; y < grid_size ; y ++ ) {
      for( z = 0 ; z < grid_size ; z ++ ) {
        if( surface_grid[gaddress(x,y,z,grid_size)] == (fftw_real)internal_value ) elec_grid[gaddress(x,y,z,grid_size)] = (fftw_real)0 ;
      }
    }
  }
/************/
  return ;
}
