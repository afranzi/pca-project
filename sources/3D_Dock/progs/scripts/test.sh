#!/bin/bash

if (test $1 -eq 1) then
  PROG="./../ftdock -static ./../inputs/proteins/2pka.parsed -mobile ./../inputs/proteins/5pti.parsed"
elif (test $1 -eq 2) then
  PROG="./../ftdock -static ./../inputs/proteins/1hba.parsed -mobile ./../inputs/proteins/5pti.parsed"
elif (test $1 -eq 3) then
  PROG="./../ftdock -static ./../inputs/proteins/4hhb.parsed -mobile ./../inputs/proteins/5pti.parsed"
else
  echo "Nombre de test incorrecte... fi d'execució de l'script"
  exit
fi

echo Test $1
original="./../outputs/test$1.output"
time $PROG > /tmp/test$1
diff /tmp/test$1 $original

echo -e "\n---------------------\n\nTest $1 finalitzat correctament\n"